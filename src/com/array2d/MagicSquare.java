package com.array2d;

import java.util.Scanner;

public class MagicSquare {
    int[][] array;
    int rows;
    int columns;
    public void initializeArray(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the Rows:- ");
        rows=scanner.nextInt();
        System.out.println("Enter the Columns:- ");
        columns=scanner.nextInt();
        array=new int[rows][columns];
        System.out.println("Enter the ArrayElements:- ");
        for (int iterate = 0; iterate < rows; iterate++) {
            for (int iterate2 = 0; iterate2 < columns; iterate2++) {
                array[iterate][iterate2]=scanner.nextInt();
            }
        }
    }

    public boolean checkMagicSquare(){
        int size=array.length;
        int sum=0;
        int rightDiagonalSum=0;
        int leftDiagonalSum=0;
        for (int iterate = 0; iterate < rows; iterate++) {
            sum+=array[0][iterate];
        }
        for (int iterate1 = 0; iterate1 < rows; iterate1++) {
            int rowsSum=0;
            int columnSum=0;
            for (int iterate2 = 0; iterate2 < columns; iterate2++) {
                rowsSum+=array[iterate1][iterate2];
                columnSum+=array[iterate1][iterate2];
            }
            if(rowsSum != sum || columnSum != sum){
                return false;
            }
        }
        for (int iterate3 = 0; iterate3 < size; iterate3++) {
            leftDiagonalSum += array[iterate3][iterate3];
            rightDiagonalSum += array[iterate3][size - iterate3 - 1];
        }
        if (leftDiagonalSum != sum || rightDiagonalSum != sum) {
            return false;
        }
        return true;
    }
}
