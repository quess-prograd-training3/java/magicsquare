import com.array2d.MagicSquare;

public class Main {
    public static void main(String[] args) {
        MagicSquare magicSquare=new MagicSquare();
        magicSquare.initializeArray();
        if(magicSquare.checkMagicSquare()){
            System.out.println("Yes");
        }
        else{
            System.out.println("No");
        }
    }
}